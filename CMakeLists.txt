cmake_minimum_required(VERSION 3.21)
project(additional_2_3)

set(CMAKE_CXX_STANDARD 20)

add_executable(additional_2_3 main.cpp fun.hpp fun.cpp)
