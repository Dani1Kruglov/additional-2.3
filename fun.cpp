#include <iostream>

#include "fun.hpp"


Dlist::Dlist(){
    p = new list;
    p -> next = p;
    p -> prev = p;
    head = nullptr;
    tail = nullptr;
    num = 0;

}

void Dlist::ADDHEAD(int age){

    list* tmp = new list;
    tmp->next = p -> next;
    tmp->prev = p;
    p -> next -> prev = tmp;
    p -> next = tmp;
    tmp->age = age;
    num++;

}

void Dlist::ADDTAIL(int age){
    list* tmp = new list;
    tmp->prev = p -> prev;
    tmp->next = p;
    p -> prev -> next = tmp;
    p -> prev = tmp;
    tmp -> age = age;
    num++;
}

void Dlist::INSERT(int pos, int age){
    if (pos < 1 || pos > num + 1) {
        std::cout << "Этой ячейки не существует, введите номер другой ячейки";
        std::cin >> pos;
        std::cout << std::endl;
    }
    if (pos == num + 1){
        ADDTAIL(age);
        return;
    }

    if (pos == 1){
        ADDHEAD(age);
        return;
    }

    int i = 0;

    list* curr = p;

    while (i < pos){
        curr = curr->next;
        i++;
    }

    list* prevcurr = curr->prev;
    list* tmp = new list;
    tmp->age = age;

    if (prevcurr != nullptr && num != 1)
        prevcurr->next = tmp;

    tmp->next = curr;
    tmp->prev = prevcurr;
    curr->prev = tmp;
    num++;
}


void Dlist::DELETE(int pos){

    if (pos < 1 || pos > num){
        std::cout << "Этой ячейки не существует, введите номер другой ячейки";
        std::cin >> pos;
        std::cout << std::endl;
    }
    int i = 1;

    list* Del = p->next;

    while (Del != p && i < pos){
        Del = Del->next;
        i++;
    }


    Del->next->prev = Del->prev;
    Del->prev->next = Del->next;

    delete Del;
    num--;

}

void Dlist::PRINT(){

    if (num != 0){
        list* tmp = p -> next;
        while (tmp != p){
            std::cout << tmp -> age  << " ";
            tmp = tmp->next;
        }
    }
}

void Dlist::REVERSEPRINT(){
    if (num != 0){
        list* tmp = p ->prev;
        while (tmp != p){
            std::cout << tmp->age << " ";
            tmp = tmp->prev;
        }
    }
}

void Dlist::CLEAR(){

    list* tmp;
    list* a = p->next;

    while (a != p){
        tmp = a;
        a = a->next;
        delete tmp;
    }
}

Dlist::~Dlist(){
    CLEAR();
    delete p;
}






