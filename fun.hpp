#ifndef ADDITIONAL_2_3_FUN_HPP
#define ADDITIONAL_2_3_FUN_HPP

#endif


#pragma once

#include <iostream>


struct list{
    list* next;
    list* prev;
    int age;
};


class Dlist{
    list* head;
    list* tail;
    list* p;
    int num;

public:
    Dlist();
    ~Dlist();
    void ADDHEAD(int age);
    void ADDTAIL(int age);
    void INSERT(int pos, int age);
    void REVERSEPRINT();
    void DELETE(int pos);
    void PRINT();
    void CLEAR();
};


